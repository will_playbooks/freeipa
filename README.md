Freeipa  
=========  
  
A ansible role for FreeIPA (Master Node) instalation
  
Requirements  
------------  
  
Ansible installed  
  
Default Variables  
--------------  
  
### FreeIPA Role
**domain:** ipa.example.com  
**realm:** IPA.EXAMPLE.COM  
**dm_pass:** P@ssw0rd  
**admin_pass:** P@ssw0rd  
  
Dependencies  
------------  
  
None  
  
Example Playbook  
----------------  
  
    - hosts: servers  
      roles:  
         - { role: freeipa }  
  
License  
-------  
  
BSD  